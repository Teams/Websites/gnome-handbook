Issue Management
================

This page provides information on how to systematically review sets of issues, in order to help manage a GNOME issue tracker. It also includes communication guidelines for those who carry out issue management work.

:doc:`Additional issue management guidelines </maintainers/issue-management>` are available elsewhere for those who are in charge of issue trackers.

Reviewing sets of issues
------------------------

Regular review of large sets of issues is key to keeping an issue tracker under control. The goal of this activity is to close issues which aren't useful and to improve the quality of those which are kept open. To do this, the first step is to select a set of issues to focus on. This will typically be issues that:

* Have been recently reported
* Have existed for the longest time
* Haven't seen activity for the longest time
* Are labelled with **Needs Information** or **Needs Design**

Once a tracker's issues have been reviewed using these filters, others can be tried, such as those for specific sub-components, features, and issue types. This can be a good way to identify duplicates and related issues.

The :doc:`review process <review>` describes what to look for when reviewing each issue, as well as the actions that can be taken for each one.

.. _contributing issue review:

How to contribute
-----------------

Helping to manage GNOME's issue trackers is a fantastic way to contribute, by allowing GNOME's developers to be more effective in their roles. Some issue review steps can only be performed by experienced contributors, and permissions for certain actions in the issue tracker are restricted. However, many issue review tasks can be done by anyone. This includes:

* Finding duplicate issues
* Identifying issues that are filed against an incorrect component
* Confirming whether you can or can't reproduce newly created issues on your system
* Confirming whether old issues can or can't be reproduced using a recent GNOME version

You can do each of these activities without needing special permission. In each case, just comment on the issue with your findings. The maintainers of the component will see your comment and can take appropriate action. They will also thank you for it!

Another way to get started with issue management activities is to speak directly to a module's maintainer: decide which module you are interested in, familiarize yourself with its issue tracker (assuming you haven't already), and then speak to the maintainer about how you could help with the issue tracker. Often they will be able to help you get started.

The issue review guidelines in this handbook will give you a good start in how to undertake this work.

.. _communication guidelines:

Communication guidelines
------------------------

The issue tracker is one of the main ways that the GNOME project interfaces with the outside world. Therefore, while there is a need to be efficient in our interactions within the issue tracker, it is also necessary to be considerate in our communications.

It is important to remember that everyone who reports an issue is a human being. These people have usually encountered a problem, which they are trying to resolve in the best way they know how. It is also important to recognize that the people who report issues today include the contributors of tomorrow.

The following guidelines are recommended when communicating via the issue tracker:

* Always be polite. In particular, always thank someone for taking the time to report an issue (this is especially true when closing it). It takes a lot of effort for an inexperienced contributor to file an issue, so by the time you read it they may have already invested significantly in writing it.
* Apologize if it hasn't been possible to resolve an issue, or if it hasn't received attention. For example, you can say: "I'm sorry that this issue has not received a response until now." or "Sorry that you've had this problem."
* If you disagree with the other person's view, it is a good idea to show that you still recognize their personal experience. "I'm sorry you had that experience" or "I can see how that would be frustrating" are good phrases to use when you disagree with a reporter.
* When closing an issue:
   * Always provide an explanation. This explanation doesn't have to be long or involved, but it should exist. For example, a simple "Sorry you've experienced this issue. Unfortunately, it isn't practical to resolve it with the current architecture." will often be enough.
   * Don't be afraid to suggest that the issue could be reopened in the future. This helps to avoid confrontation and can be useful in ambiguous cases. For example, "Closing as this doesn't seem to be reproducible; we can re-open if there's new information" can go a long way.

The :doc:`stock responses </issues/stock-responses>` are a good guide for the kind of tone to use on the issue tracker.
