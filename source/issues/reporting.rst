Issue Reporting
===============

Problems with GNOME's software can be reported using each module's issue tracker in GitLab. Issue reports should be created in accordance with the following guidelines.

General guidelines
------------------

* GNOME only accepts issue reports for the two most recent stable releases or the current development version. If the GNOME version that you are using is no longer supported, you can try to reproduce the issue in a newer version. Virtual machines can be a useful way to do this.
* Issue reports should be factual and descriptive, and should stick to the technical and design issues at hand. Please avoid emotive language or judgments (this includes expressions of upset, anger, outrage or disappointment). All interactions on the issue tracker should follow the `Code of Conduct <https://conduct.gnome.org/>`_.
* Each issue report should restrict itself to a single problem or feature request. If you have more than one problem, please create a separate report for each one.
* When writing reports, describe your personal experience rather than arguing your case in general or abstract terms. Try to avoid proposing solutions or making requests without providing this background information.
* Be realistic in your expectations. The GNOME project receives more issue reports than it has the resources to resolve, so be aware that your issue may not be fixed. Additionally, developers may not be able to respond to your report straight away.

Issue reporting steps
---------------------

Issue reports are created using `GNOME's GitLab <https://gitlab.gnome.org/>`_:

* In GitLab, find the software project for your issue by searching or browsing the `list of GNOME projects <https://gitlab.gnome.org/GNOME>`_. If you are unsure which module to report your issue against, you can ask for advice in :doc:`Matrix </get-in-touch/matrix>` or :doc:`Discourse </get-in-touch/discourse>`, or try searching for similar `issues in the GNOME group of projects <https://gitlab.gnome.org/groups/GNOME/-/issues>`_.
* Once you have opened the module, select **Issues** in the sidebar.
* Check whether the issue has already been reported, by searching for different terms relevant to your issue. (When doing this, make sure to look at search results for both open and closed issues.) If you find an existing report for your issue:
   * If the report is for an older GNOME version, you can add a comment to state that the issue exists in the version you are using.
   * You can add a thumbs up (+1) to the original issue report and you can also subscribe to the issue to receive updates.
* If you don't find an existing report for your issue, proceed to create a new issue by clicking the blue **New issue** button at the top of the page. If you are not signed in already, you will be prompted to do so.

Writing the report
------------------

High-quality reports are easier to process, diagnose and resolve. Therefore, please take time and care when creating issues, by following the following guidelines:

* Avoid broad and ambiguous titles. Instead, give your issue a descriptive title which includes specifics.
   * Example: instead of writing: "Online accounts is not working", write "Browser window is blank/empty when adding a Nextcloud online account".
* The body of the issue should include a detailed description. This should include:
   * A description of what happened, written as a series of steps. This should include the actions you took, what you expected to happen, and what actually happened. Try to tell it as a story from your personal perspective.
   * How often the issue occurs. Does it happen every time, or only sometimes? Does it only happen under particular conditions?
* Issue reports should also include:
   * The version of the component where the problem occurred.
   * The distribution and its version.
   * If the problem you are reporting can be seen on the screen, a screenshot or a screen recording. (When taking these, please ensure that your system is in US English.)
   * If the report is of a crash, it should ideally include a :doc:`stack trace </issues/stack-traces>` (without this, it will be hard to fix).

Feature requests
----------------

Most issues describe problems: either something that isn't working correctly or which is not as easy to use as it could be (these are often referred to as "bugs"). However, in some cases a reporter may want to use an issue to request a feature or functionality which is currently missing.

Guidelines for doing this:

* Some GNOME modules require feature requests to be made in a forum other than the issue tracker. It is recommended to check the module's GitLab page and issue template to see whether this is the case.
* When requesting new features, it is important to provide background information about why you want the feature. This may include information about:
    * Why the current feature set isn't sufficient
    * Practical factors from your environment or use case which make the feature important for you
    * Personal preferences or experience which are influencing your request
* Please be aware that new features can require significant time and effort from developers, and that feature requests need to be accommodated alongside existing development plans. Consultation with the design team is often also required.
