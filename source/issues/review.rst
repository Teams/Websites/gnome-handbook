Issue Review
============

These guidelines should be followed whenever issues are reviewed for quality, completeness and appropriateness. See the :doc:`issue management page </issues/management>` for information on when issues should be reviewed in this way.

Basic validation checks
-----------------------

Issues which fail any of the following checks should be closed:

.. list-table::
  :header-rows: 1

  * - Check
    - Additional steps
  * - Does the issue belong to GNOME?
    - Add the **Not GNOME** label. If possible, include advice about where the issue should be reported.
  * - If the issue is a feature request: does the module accept feature requests as issues?
    - Point the reporter in the direction of the appropriate forum
  * - If the issue is a feature request and the module does accept feature requests in its issue tracker: is the feature wanted?
    - Add the **Out of Scope** label
  * - Is the bug actually a bug? (In other words, is the behavior being reported intentional/desirable?)
    - Add the **Expected Behavior** label
  * - Has the issue had the **Needs Information** label for longer than 6 weeks?
    - Add the **Not Actionable** label

When closing issues, please follow the :ref:`communication guidelines <communication guidelines>`.

Incorrect component
-------------------

If the issue has been reported against the wrong module:

* If you're able, move it to the correct module
* If you aren't able to move the issue to the correct module, close the issue and try to provide advice on where it should be reported

Addressing quality issues
-------------------------

Quality issues with issue reports can include any of the following:

* It is difficult or impossible to understand the problem that's being described
* The title is difficult to understand or does not accurately describe the issue
* The report describes more than one problem
* Missing information, such as:
   * Distribution name and version
   * Module version
   * For UI issues: screenshot or screen recording (in US English)
   * For crashes: :doc:`a stack trace </issues/stack-traces>`

Steps that reviewers should take to resolve these issues:

* Ask the reporter to clarify the problem (add the **Needs Information** label when doing this)
* Improve the issue title by rewriting it
* If the report describes multiple problems or enhancements:
   * Pick one of the problems and add a comment to say that 1) this will be the sole focus of the report, and 2) ask that other issues be reported separately.
   * Add the **Needs Information** label and ask the reporter to clarify which single issue the report is about.
* If any information is missing, request it and add the **Needs Information** label

Applying issue labels
---------------------

When reviewing issues, ensure that the correct labels are applied, in particular (see `the label descriptions <https://gitlab.gnome.org/groups/GNOME/-/labels>`_):

* Bug
* Cleanup
* Crash
* Enhancement
* Feature
* Regression
* Performance
* Regression
* Security
* Help wanted
* Newcomers
