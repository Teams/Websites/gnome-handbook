.. meta::
   :copyright: Copyright 2025 GNOME Foundation, Inc.
   :license: CC-BY-SA-4.0
..
   This has to be duplicated from above to make it machine-readable by `reuse`:
   SPDX-FileCopyrightText: 2025 GNOME Foundation, Inc.
   SPDX-License-Identifier: CC-BY-SA-4.0

Release Signing
===============

In order to prove that a git tag for a release was made by a maintainer of a module, and it has not been modified by a malicious party since, it is important to sign the tag.

While GitLab currently provides guarantees about source code integrity for GNOME, the git repository itself is the canonical store of source code, and releases tagged in it will probably outlast GNOME’s use of GitLab.

In order to sign a release, the maintainer making the release needs a GPG key set up with git, and the public half of that key needs to be published. The private half of the key must be kept private, and not even shared with other GNOME developers.

Currently, git supports both GPG and SSH keys to sign tags — but GitLab doesn’t recognize those signatures. GitLab may gain this feature in future.

You may choose to use either a GPG or an SSH key, both are equivalent. This documentation uses GPG keys, as the git tooling for them is more mature. Some maintainers may prefer to use an SSH key — that’s supported, but they must find a way to publish the public half of their key.

The guarantees provided by signing a release depend on the maintainer’s key remaining private (proving that the release could only be made by them) and consistent (proving that multiple releases were made by the same person). Hence it is important to only generate one key, and use it over the course of multiple releases. Do not generate a new key for each release.

Preparation for signing releases
--------------------------------

In order to sign a release, the commit for that version needs to be signed (see :doc:`making a release </maintainers/making-a-release>`). You can also choose to sign all your commits. To set this up, follow `GitLab's instructions <https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html>`_ to:

* create a GPG key, using the same email address as you use to author your Git commits
* add your GPG key to your gitlab.gnome.org account, and
* associate your GPG key with Git.

This only needs to be set up once, and then the key should be re-used for signing all future releases.

It is also a good idea to run ``gpg --send-keys <key ID>`` to send the public half of your key to a public keyserver, where anyone can query it.

Once you have completed these steps, you are ready to create signed releases, as described in :doc:`making a release </maintainers/making-a-release>`.

Verifying releases
------------------

In order to verify a release, first get the public half of the maintainer’s key used to sign the release. If the key was published to gitlab.gnome.org, the public part can be retrieved using ``gpg --fetch-keys https://gitlab.gnome.org/<username>.gpg``, where ``username`` is the maintainer’s gitlab.gnome.org username.

There are two main ways to verify a release: ``git evtag`` and ``git tag``. Which method you use to verify the release is determined by how the release was signed.

Releases signed using `git-evtag <https://github.com/cgwalters/git-evtag>`_ have a ``Git-EVTag`` paragraph in the tag message. They can be verified using:

::

  git checkout <tag>
  git evtag verify <tag>

If the tag message does not include ``Git-EVTag``, it can be verified using:

::

  git tag -v <tag>

Releases signed using ``git-evtag`` can also be verified this way, but with fewer guarantees.
