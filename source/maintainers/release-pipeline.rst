Release Pipeline
================

To make a release, a `GitLab CI/CD <https://docs.gitlab.com/ee/ci/>`__ pipeline needs to be set up in the project repository. This pipeline needs to be able to generate a release tarball and upload it to the GNOME server. The following instructions will help you set up a new release pipeline for a new GNOME project that does not have one already.

Place a ``.gitlab-ci.yml`` file containing the CI/CD pipeline configuration in the root of the project (`learn more <https://docs.gitlab.com/ee/ci/#step-1-create-a-gitlab-ciyml-file>`__). The configuration is project-specific and should be tailored to the project's needs, but it is highly advised to utilize a `CI/CD component prepared by the GNOME Release Team <https://gitlab.gnome.org/GNOME/citemplates/-/blob/master/templates/release-service.yml>`__.

Example of a minimal ``.gitlab-ci.yml`` file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  include:
    - component: "gitlab.gnome.org/GNOME/citemplates/release-service@master"
      inputs:
        job-stage: "release"
        dist-job-name: "build-release-tarball"              # <1.>
        tarball-artifact-path: "${TARBALL_ARTIFACT_PATH}"   # <2.>

  stages:
    - "build"
    - "release"

  variables:
    MESON_BUILD_DIR: "_build"
    TARBALL_ARTIFACT_PATH: "${MESON_BUILD_DIR}/meson-dist/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}.tar.xz"

  build-release-tarball:                                    # <3.>
    stage: "build"
    script:
      - meson setup "${MESON_BUILD_DIR}"
      - meson dist -C "${MESON_BUILD_DIR}" --include-subprojects
    artifacts:
      name: "${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
      when: "always"
      paths:
        - "${TARBALL_ARTIFACT_PATH}"

#. Makes it so the release job is triggered only after the build job finishes and is successful.
#. The path to the release tarball artifact.
#. The job that generates the release tarball. Will differ per project. You can use a pre-existing job that creates a release tarball.

Steps taken by the pipeline:

#. The pipeline defines global variables.
#. The pipeline includes the release service component and sets the necessary inputs.
#. The pipeline builds the project, generates the release tarball and uploads it as an artifact.
#. The included component inner workings are triggered, which will upload the tarball to the GNOME server.

If your application is already using the `Flatpak CI template <https://gitlab.gnome.org/GNOME/Initiatives/-/wikis/DevOps-with-Flatpak>`__, it already creates a Release Tarball for you and you will only need to define ``${TARBALL_ARTIFACT_PATH}`` instead.

Example of the ```.gitlab-ci.yml`` for gnome-font-viewer

::

  include:
    - project: "gnome/citemplates"
      file: "flatpak/flatpak_ci_initiative.yml"
    - component: "gitlab.gnome.org/GNOME/citemplates/release-service@master"
      inputs:
        dist-job-name: "flatpak"
        tarball-artifact-path: "${TARBALL_ARTIFACT_PATH}"

  variables: #<1.>
    FLATPAK_MODULE: "gnome-font-viewer"
    TARBALL_ARTIFACT_PATH: ".flatpak-builder/build/${FLATPAK_MODULE}/_flatpak_build/meson-dist/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}.tar.xz"

  flatpak:
    extends: '.flatpak'
    variables:
      MANIFEST_PATH: 'build-aux/flatpak/org.gnome.font-viewerDevel.json'
      RUNTIME_REPO: 'https://sdk.gnome.org/gnome-nightly.flatpakrepo'
      APP_ID: 'org.gnome.font-viewerDevel'
      BUNDLE: 'org.gnome.font-viewerDevel.flatpak'

  nightly:
    extends: '.publish_nightly'
    dependencies: ['flatpak']
    needs: ['flatpak']

#. Make sure that your ``TARBALL_ARTIFACT_PATH`` and ``FLATPAK_MODULE`` variables are on the same scope, so Gitlab will expand the nested variables correctly.


The release process is possible only with a pipeline that has been triggered by a protected Git tag. The tag is used as a trigger to generate and name the tarball. The tarball is then uploaded to the GNOME server. This limitation lies in the security constraints of the underlying service that is used to upload the tarball.


Requirements for the GitLab project settings and pipeline invocation
--------------------------------------------------------------------

* The project must be hosted on `GNOME GitLab <https://gitlab.gnome.org/>`__.
* The project must be part of the `GNOME <https://gitlab.gnome.org/GNOME>`__ or `Incubator <https://gitlab.gnome.org/GNOME/Incubator>`__ group.
* The pipeline must be triggered by a tag.
* The tag must be `protected <https://docs.gitlab.com/ee/user/project/protected_tags.html>`__ so that only maintainers can create one. This is done automatically for all projects in the GNOME group.

* The release tarball name has to be in the format ``$CI_PROJECT_NAME-$CI_COMMIT_TAG.tar.xz``, where ``$CI_PROJECT_NAME`` is the project name  and ``$CI_COMMIT_TAG`` is the tag name (`learn more <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`__).
