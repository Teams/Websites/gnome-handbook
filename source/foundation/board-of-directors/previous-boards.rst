Previous Boards
===============

Details about each election, including candidate affiliations, can be found on the `vote.gnome.org <http://vote.gnome.org>`_.

July 2024 to June 2025
----------------------

* Allan Day
* Erik Albers
* Federico Mena Quintero
* Julian Sparber
* Karen Sandler
* Michael Downey (resigned mid-term)
* Pablo Correa
* Philip Chimento (appointed mid-term)
* Robert McQueen

July 2023 to June 2024
----------------------

* Erik Albers
* Jeremy Allison
* Michael Downey
* Sammy Fung
* Robert McQueen
* Regina Nkenchor
* Sonny Piers

July 2022 to July 2023
----------------------

* Jeremy Allison
* Martin Abente
* Regina Nkenchor
* Robert McQueen
* Sammy Fung
* Thibault Martin
* Philip Chimento

June 2021 to June 2022
----------------------

* Alberto Ruiz (resigned mid-term)
* Ekaterina Gerasimova
* Felipe Borges
* Martin Abente (appointed mid-term)
* Regina Nkenchor
* Robert McQueen
* Thibault Martin
* Philip Chimento

June 2020 to June 2021
----------------------

* Allan Day
* Ekaterina Gerasimova
* Federico Mena Quintero
* FelipeBorges
* Philip Chimento
* Regina Nkenchor
* Robert McQueen

June 2019 to June 2020
----------------------

* Allan Day
* Britt Yazel
* Carlos Soriano
* Federico Mena Quintero
* Philip Chimento
* Robert McQueen
* Tristan Van Berkom

July 2018 to June 2019
----------------------

* Philip Chimento
* Allan Day
* Kat Gerasimova
* Rob McQueen
* Federico Mena Quintero
* Nuritzi Sanchez
* Carlos Soriano

July 2017 to June 2018
----------------------

* Zeeshan Ali (left mid-term)
* Cosimo Cecchi
* Allan Day
* Alexandre Franke
* Meg Ford
* Didier Roche (joined mid-term)
* Nuritzi Sanchez
* Carlos Soriano

July 2016 to June 2017
----------------------

* Allan Day
* Cosimo Cecchi
* Shaun McCance
* Nuritzi Sanchez
* Alexandre Franke
* Jim Hall
* Meg Ford

July 2015 to June 2016
----------------------

* Allan Day
* Andrea Veri
* Alexandre Franke ('''See Note''')
* Cosimo Cecchi
* Ekaterina Gerasimova
* Jeff Fortin
* Shaun McCance

Christian Hergert was elected, but resigned in October. Alexandre Franke has been nominated to replace Christian Hergert as director. See `welcome to Alexandre Franke, new board member <https://mail.gnome.org/archives/foundation-announce/2015-November/msg00004.html>`_.

July 2014 to June 2015
----------------------

* Sriram Ramkrishna
* Ekaterina Gerasimova
* Karen Sandler
* Tobias Mueller
* Andrea Veri
* Marina Zhurakhinskaya
* Jeff Fortin

July 2013 to June 2014
----------------------

* Tobias Mueller
* Joanmarie Diggs
* Emmanuele Bassi
* Andreas Nilsson
* Sriram Ramkrishna
* Ekaterina Gerasimova
* Marina Zhurakhinskaya.

July 2012 to June 2013
----------------------

* Bastien Nocera
* Emmanuele Bassi
* Andreas Nilsson
* Joanmarie Diggs
* Tobias Mueller
* Shaun McCance
* Seif Lotfy

July 2011 to June 2012
----------------------

*  Brian Cameron
*  Emmanuele Bassi
*  Ryan Lortie
*  Shaun McCance
*  Bastien Nocera
*  Stormy Peters
*  Germán Póo-Caamaño

July 2010 to June 2011
----------------------

* Brian Cameron
* Emily Chen
* Paul Cutler 
* Og Maciel
* Germán Póo-Caamaño
* Andreas Nilsson
* Bastien Nocera

July 2009 to June 2010
----------------------

* Brian Cameron
* Jorge Castro [1]
* Paul Cutler [2]
* Diego Escalante Urrelo
* Germán Póo-Caamaño
* Srinivasa Ragavan
* Vincent Untz

[1] Lucas Rocha was elected, but resigned in February 2010. Jorge Castro has been nominated to replace Lucas Rocha as director. See `resigning from the board (February 2010) <http://mail.gnome.org/archives/foundation-announce/2010-February/msg00002.html>`_.

[2] Behdad Esfahbod was elected, but resigned in March 2010. Paul Cutler has been nominated to replace Behdad Esfahbod as director. See `stepping down from the board <http://mail.gnome.org/archives/foundation-announce/2010-March/msg00002.html>`_.

January 2008 to June 2009
-------------------------

In September/October 2007, it was decided that the next board (and only the next one) will serve for 18 months, in order to align the start of the new board with GUADEC. Read `the next board will serve for a period of 18 month starting January 1st 2008 <http://mail.gnome.org/archives/foundation-announce/2007-October/msg00002.html>`_.

* Brian Cameron
* Diego Escalante Urrelo [1]
* Behdad Esfahbod
* John Palmieri
* Lucas Rocha
* Vincent Untz
* Luis Villa

[1] Jeff Waugh was elected, but resigned in December 2008. Diego Escalante Urrelo has been nominated to replace Jeff Waugh as director. See `changes to the GNOME board <http://mail.gnome.org/archives/foundation-list/2008-December/msg00014.html>`_.

Fall 2006 to Fall 2007
----------------------

* Behdad Esfahbod
* Glynn Foster
* Quim Gil
* Anne Østergaard
* Lucas Rocha [1]
* Vincent Untz
* Jeff Waugh

[1] Dave Neary was elected, but resigned in July 2007. Lucas Rocha has been nominated to replace Dave Neary as director. See `resigning from the board (July 2007) <http://mail.gnome.org/archives/foundation-list/2007-July/msg00025.html>`_.

Fall 2005 to Fall 2006
----------------------

In October/November 2005, a vote was held to reduce the number of board directors from 11 to 7. See `reducing board size referendum results are now official <http://mail.gnome.org/archives/foundation-announce/2005-November/msg00009.html>`_.

* Jonathan Blandford
* Quim Gil ('''See Note''')
* Federico Mena-Quintera
* David Neary
* Anne Østergaard
* Vincent Untz
* Jeff Waugh

'''Note:''' Luis Villa was elected, but resigned in June 2006. Quim Gil has been nominated to replace Luis Villa as director. See `co-option of Quim Gil to the board <http://mail.gnome.org/archives/foundation-announce/2006-June/msg00000.html>`_.

Fall 2004 to Fall 2005
----------------------

* Jonathan Blandford
* Murray Cumming
* Jody Goldberg
* Miguel de Icaza
* Federico Mena-Quintera
* David Neary
* Tim Ney
* Christian Schaller
* Owen Taylor (Chairperson)
* Daniel Veillard
* Luis Villa

Fall 2003 to Fall 2004
----------------------

* Jonathan Blandford
* Dave Camp
* Glynn Foster
* Nat Friedman
* Jody Goldberg
* Bill Haneman
* Leslie Proctors
* Owen Taylor
* Malcolm Tredinnick
* Luis Villa
* Jeff Waugh

Fall 2002 to Fall 2003
----------------------

* Jonathan Blandford
* Miguel de Icaza
* Glynn Foster
* Nat Friedman
* Jim Gettys
* Jody Goldberg
* Bill Haneman
* James Henstridge
* Daniel Veillard
* Luis Villa
* Jeff Waugh

Fall 2001 to Fall 2002
----------------------

* Jonathan Blandford
* Miguel de Icaza
* Nat Friedman
* Jim Gettys
* Jody Goldberg
* Telsa Gwynne
* James Henstridge
* George Lebl
* Federico Mena-Quintero
* Havoc Pennington
* Daniel Veillard

Fall 2000 to Fall 2001
----------------------

* Bart Decrem
* Jim Gettys
* John Heard
* Miguel de Icaza
* Raph Levien
* Dan Mueth
* Havoc Pennington
* Federico Mena Quintero
* Maciej Stachowiak
* Owen Taylor
* Daniel Veillard
