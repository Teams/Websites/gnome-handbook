Officers
========

The following officer positions are formal roles which are appointed by the board. Some are required by the GNOME Foundation bylaws, while others are optional. Officer positions do not have to be held by directors, though in practice they typically are.

President
~~~~~~~~~

* This is a required position
* Acts as the chair if no chair is assigned
* Acts as the Executive Director, if there isn't one
* Has contract signing and spending authorization powers
* Is the link between the board and the Executive Director, and acts as the ED's line manager
* Sits on the compensation, executive and finance committees
* This post is currently held by Robert McQueen

Vice-President
~~~~~~~~~~~~~~

* This is a required position
* Stands in for the President in their absence
* This post is currently held by Allan Day

Chair
~~~~~

* This is an optional position
* Prepares an agenda and sends it to the board before each meeting
* Chairs each board meeting
* Sits on the governance committee
* This post is currently held by Allan Day

Treasurer
~~~~~~~~~

* This is a required position
* Ensures board oversight of the Foundation's financials
* Regularly reports to the board on the Foundation's finances (cash, receivables, outstanding bills, expenditures, income)
* Sits on the compensation and finance committees
* This post is currently held by Michael Downey

Assistant Treasurer
~~~~~~~~~~~~~~~~~~~

* This is an optional position
* Assists the treasurer
* This post is held by Pablo Correa Gomez

Secretary
~~~~~~~~~

* This is a required position
* Takes minutes of board and advisory board meetings
* Publishes meeting minutes
* Takes note of actions and discussion since the preceding meeting (such as items discussed on the board mailing list) and ensures they are included in the meeting minutes.
* Strives to keep information (such as process documentation) organized.
* This post is currently held by Julian Sparber

Vice-Secretary
~~~~~~~~~~~~~~

* This is an optional position
* Acts as the secretary if they are absent, and generally helps with secretarial duties
* This post is currently held by Federico Mena Quintero

Operations Officer
~~~~~~~~~~~~~~~~~~

* This is an optional position
* Oversees GNOME Foundation operations and signs paperwork on behalf of the Foundation
* This post is currently held by Richard Littauer
