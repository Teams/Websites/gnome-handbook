Compensation Committee
======================

The compensation committee's role to set the salary of the GNOME Foundation Executive Director. It is also responsible for carrying out performance reviews.

When it is in operation, the committee is required to have at least three members, made up by members of the Board of Directors. It must include the president and treasurer.

Committee members:

* Robert McQueen (Board President, ex officio)
* Michael Downey (Board Treasurer, ex officio)
* Allan Day
