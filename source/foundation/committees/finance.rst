Finance Committee
=================

Current members:

* MichaelDowney, Chair (Board Treasurer, ex officio)
* RichardLittauer (Executive Director, ex officio)
* RosannaYuen (Director of Operations, ex offico)
* PabloCorreaGomez (Board Vice-Treasurer) 

Committee Charter
-----------------

The following was approved by the Board of Directors on 10 February 2021.

The Finance Committee is a standing committee of the GNOME Foundation Board of Directors. It assists the Board with monitoring and overseeing the finances of the Foundation, including budget planning and financial policies.

Key responsibilities
~~~~~~~~~~~~~~~~~~~~

The responsibilities of the Finance Committee include:

* Preparing financial reports to the board,
* Regular review of the Foundation's finances, including spending, balances and actuals with regards to the budget,
* Assisting staff in the preparation of the Annual Budget, and
* Providing periodic finance reports to the Board. 

Committee composition
~~~~~~~~~~~~~~~~~~~~~

The Finance Committee consists of at least three, and no more than five members, and must include the current Treasurer, Executive Director, and (if applicable) senior executive responsible for Finance, such as CFO, VP or Director of Finance or Operations.

Committee members are to be appointed by the Board of Directors as set out in the Foundation Bylaws.

The Finance Committee is chaired by the Board Treasurer.

Committee meetings
~~~~~~~~~~~~~~~~~~

The Finance Committee meets no less than quarterly, during the month following the close of each fiscal quarter, e.g., January, April, July, October. During these quarterly meetings, the Committee will review quarterly financial reports, and other financial reports that may be required. Reports will be prepared for the Foundation Board to summarize these reviews along with any other recommendations for Board consideration.

The Committee will further determine interim meetings that may need to happen from time to time, and will provide at least 1 week notice before any non-emergency Committee meeting.

Authorization and limitations of power
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Finance Committee has no power or authority to act on behalf of the full Board of Directors.
