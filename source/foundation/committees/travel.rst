Travel Committee
================

The GNOME Travel Committee is responsible for processing sponsorship requests for hackfests and conferences. This includes:

* Receive the annual travel budget from the Board of Directors
* Receive and process applications
* Receive and process the receipts for travel and/or accommodation
* Instruct the Director of Operations to process the reimbursements

For sponsorship requests, please refer to the :doc:`travel policy </events/travel>`.
For other confidential requests, the committee should be contacted through the travel-committee@gnome.org ticket list.
You can also chat with the committee via Matrix at `#travel:gnome.org <https://matrix.to/#/#travel:gnome.org>`_.

Current committee
-----------------

* Meg Ford
* Rosanna Yuen
* Umang Jain
* Asmit Malakannawar
* Melissa Wu
* Anisa Kuci
* Julian Sparber
