Executive Committee
===================

Current members:

* Robert McQueen (President of the Board; Chair of the Executive Committee)
* AllanDay (Chair of the Board)
* Julian Sparber (Secretary of the Board)
* Richard Littauer (Executive Director) 
* Rosanna Yuen (Senior Director of People)

Committee Charter
-----------------

The following charter was approved by the board on 12 November 2024.

The Executive Committee is a standing committee of the GNOME Foundation Board of Directors. It performs oversight of the business and affairs of the GNOME Foundation, and exercises the powers of the GNOME Foundation Board of Directors between board meetings.

Key responsibilities
~~~~~~~~~~~~~~~~~~~~

The responsibilities of the Executive Committee include:

* Responding to urgent and time-sensitive situations as needed
* Performing regular oversight of the business and affairs of the GNOME Foundation
* Assisting in the planning and preparation of board agendas 

The committee reports to the board at each board meeting.

Committee composition
~~~~~~~~~~~~~~~~~~~~~

The Executive Committee is composed of a minimum of three directors, including the president. The Executive Director is automatically a committee member. 

Committee members are appointed by the Board of Directors as set out in the Foundation bylaws.

Committee meetings
~~~~~~~~~~~~~~~~~~

Executive Committee meeting requirements:

* The Executive Committee determines the frequency with which it meets. This should be no less than monthly.
* A majority of committee members constitutes quorum.
* All committee members must be invited to committee meetings directly and in writing. There is no notice period requirement for meetings.   
* Meetings may be held in person or by voice or video call. 
* Meeting minutes must record all decisions taken by the committee.

Authorization and limitations of power
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Executive Committee is empowered to act and speak on behalf of the Board of Directors. The Executive Committee derives its powers and authority from the GNOME Foundation Board of Directors. As such, the committee is subject to board decisions and cannot overrule the board.

Decision making
~~~~~~~~~~~~~~~

Formal committee decisions are only required when the Executive Director or President does not have the necessary authority.

Committee decisions can only be made at a valid meeting, and are decided by a majority vote of directors present. If a vote is tied, the Executive Director may cast a deciding vote. 

