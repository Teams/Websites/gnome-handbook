Membership Benefits
===================

A range of benefits are available to GNOME Foundation members. This page provides information on how to access them. For general information on Foundation membership, including how to apply, see `foundation.gnome.org/membership <https://foundation.gnome.org/membership/>`_.

GNOME Addresses
---------------

Email Address
~~~~~~~~~~~~~

gnome.org email addresses are provided for all Foundation members. This address is an alias which can be used as a relay for sending and receiving emails.

Aliases are created automatically within 24 hours of membership applications being accepted.

Matrix Account
~~~~~~~~~~~~~~

When logging in on Matrix using their nickname@gnome.org email alias, Foundation members receive a @gnome.org Matrix address associated with their account in the form of: ``@nickname:gnome.org``.

IRC Cloak (Libera.Chat)
~~~~~~~~~~~~~~~~~~~~~~~

Foundation members may request an IRC cloak on the Libera.Chat network. The cloak hostmask matches the pattern ``gnome/foundation/nickname``. For more details please see Libera.Chat's official documentation.

Online Services
---------------

Blog Hosting
~~~~~~~~~~~~

Foundation members are eligible to host their blog on `blogs.gnome.org <https://blogs.gnome.org/>`_. To create a blog on the platform, wait for your @gnome.org alias to be generated, then `sign up for a new blog <https://blogs.gnome.org/wp-signup.php>`_.

Planet GNOME
~~~~~~~~~~~~

Foundation members who have blogs can have them included on `planet.gnome.org <https://planet.gnome.org/>`_, whether the blog is hosted by GNOME or not. To request your blog be added, please file an issue against the `planet-web project in GitLab <https://gitlab.gnome.org/Infrastructure/planet-web/>`__. Then wait for the planet editors to review your request and eventually include your blog. 

cloud.gnome.org
~~~~~~~~~~~~~~~

GNOME hosts a Nextcloud instance at `cloud.gnome.org <https://cloud.gnome.org/>`_. This provides a range of services, including file hosting, calendaring, and contact management. Note: this service is currently restricted to GNOME-related material only (such as documents, mockups, presentations or anything else related to the GNOME project).

To use the service, just login using your GNOME Account / UID.

Video Conferencing
~~~~~~~~~~~~~~~~~~

Foundation members can use `meet.gnome.org <https://meet.gnome.org/rooms>`_ for video conferencing. To use the service, just login using your GNOME Account / UID.

Sponsorship and Discounts
-------------------------

Travel Sponsorship
~~~~~~~~~~~~~~~~~~

Foundation members are eligible for travel sponsorship to GNOME conferences and events. To apply, see the :doc:`travel policy </events/travel/sponsorship-policy>`.

Gandi Discounts
~~~~~~~~~~~~~~~

GNOME Foundation members are able to receive e-rate (up to 50% off on every service) discount at Gandi. Steps to access the discount:

#. Create a Gandi handle at https://www.gandi.net. If you have an existing account you can skip this step.
#. Set your handles' prepaid account currency accordingly to the one in use in your country.
#. Mail non-profit@gandi.net from your @gnome.org mail address indicating your GNOME Foundation membership status and Gandi handle
#. Wait for a positive reply. Your account should now have applied an E-Rate discount.
