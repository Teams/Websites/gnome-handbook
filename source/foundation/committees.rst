Committees
==========

Committees are created by the Board of Directors to carry out functions on behalf of the GNOME Foundation. The board appoints members to each committee, whose membership is required to be annually renewed. Each committee has certain powers which are delegated to it by the board.

Board committees
----------------

Standard committees which perform essential board functions.

.. list-table::
  :widths: 30 70
  :header-rows: 1

  * - Name
    - Committee Role
  * - Compensation Committee
    - Sets the salary of the Executive Director
  * - Executive Committee
    - Monitors Foundation operations and takes board-level action when required
  * - Governance Committee
    - Assesses and develops board performance
  * - Finance Committee
    - Monitors finances, prepares budgets, reports and financial policies

Non-board committees
--------------------

Committees which perform additional functions for the Foundation, which have been delegated powers by the Board of Directors.

.. list-table::
  :widths: 40 60
  :header-rows: 1

  * - Name
    - Committee role
  * - :doc:`Circle Committee </foundation/committees/circle>`
    - Runs the GNOME Circle initiative, which has consequences for Foundation membership
  * - :doc:`Code of Conduct Committee </foundation/committees/coc>`
    - Maintaining and enforcing the GNOME Project’s codes of conduct
  * - :doc:`Membership and Elections Committee </foundation/committees/membership-and-elections>`
    - Processing membership applications, maintaining the members list, conducting Foundation elections
  * - :doc:`Release Team </release-planning/release-team>`
    - Determines what is and isn't GNOME software; provides technical leadership
  * - :doc:`Travel Committee </foundation/committees/travel>`
    - Allocating travel sponsorship funding
  * - :doc:`Internship committee </foundation/committees/internship>`
    - Promoting, organizing, and conducting internship programs

Expectations for committees
---------------------------

Each committee has certain powers delegated to it by the Board of Directors. Committees must therefore meet certain minimum requirements in order for the board to be satisfied that these powers are being exercised correctly.

General expectations
--------------------

Committee members are appointed by the board until the next board annual meeting (this is separate from the AGM), at which time they must be reappointed. This is in order to avoid the situation where inactive committee members retain long-term access to personal data and other confidential materials.

Committees must be responsive to board requests and communications.

Committees must operate as a single group rather than a collection of individuals. They need to be able to speak with a single voice in communications with the community and the board. This doesn't mean that there can be no differences of opinion within a committee, but the committee members do need to adhere to whichever policy the committee decides on as a whole.

Committee chair
~~~~~~~~~~~~~~~

Each committee is required to have a chair. The minimum responsibilities of the chair are:

* Alert the board to any recommended changes in the committee membership, and keep the membership wiki page up to date.
* Alert the board if the committee is unable to perform its duties.
* Give reports to the board when requested to do so.
* Guide decision making in committee meetings, if applicable.
* Make the final decision if the committee cannot agree on a particular issue or question.

Liaison
~~~~~~~

Each committee is assigned a liaison from the board. The minimum responsibilities of the liaison are:

* Keep an eye out for needs from or problems within the committee.
* Be a central point of communication between the board and the committee.

Committee reporting
~~~~~~~~~~~~~~~~~~~

Twice a year, each committee is asked to make a report to the board. This may consist of the committee chair joining a board meeting to give a short verbal report, or it may consist of the committee chair answering a number of set questions in an email, or whatever is appropriate to the situation.

In the future, the board hopes to define a set of metrics, in consultation with the committee. Committees will be asked to report on these metrics in their reports.

Record keeping
~~~~~~~~~~~~~~

Committees must keep adequate records of their activities, which must be accessible to Foundation staff and directors. (In the case of personal data, the access can be more restricted.)

Minutes must be kept of committee meetings, and they must always be available for review when the committee is making budget or expenditure decisions.

Not all committees are required to have meetings. For procedural work carried out by a committee according to an agreed process, such as travel or membership requests, records kept within a ticket system will suffice and separate minutes are not necessary.

.. toctree::
   :hidden:

   committees/circle
   committees/coc
   committees/compensation
   committees/executive
   committees/finance
   committees/governance
   committees/internship
   committees/membership-and-elections
   committees/travel
