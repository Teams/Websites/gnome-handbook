Contact
=======

Ways to contact the infrastructure team, in order of severity:

.. list-table::
  :widths: 60 40
  :header-rows: 1

  * - Topic
    - Contact Method
  * - Emergency, urgent or severe security problem
    - `emergency@gnome.org <mailto:emergency@gnome.org>`_
  * - Security
    - `security.gnome.org <https://security.gnome.org>`_
  * - Anything else
    - `General infrastructure issue <https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template=default-template>`_

IRC/Matrix:

.. list-table::
  :widths: 30 70

  * - IRC
    - irc.libera.chat (#gnome-infrastructure)
  * - Matrix
    - gnome.element.io (#infrastructure:gnome.org)