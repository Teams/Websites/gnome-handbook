Email Aliases
=============

gnome.org domain email aliases are automatically created for every :doc:`GNOME Foundation </foundation>` member. These are just forwarding addresses and are not full email accounts - we do not host these due to limited resources.

Usage rules
-----------

gnome.org email aliases are provided for the convenience of GNOME Foundation members and to facilitate the operation of the GNOME Project. The GNOME Foundation Board retains the right to revoke aliases at any time for any reason. Types of email that are inappropriate to send with a gnome.org alias include:

* Email sent for commercial purposes.
* Email that reflects badly on the GNOME project, including, but not limited to harassing mail, offensive mail, personal attacks, and attacks on competing projects.
* Email that misleadingly attempts to portray the official positions of the GNOME project. 

Use common sense. If you have doubts whether you should send mail from your gnome.org account, use a different account. If your email might be interpreted as an official statement about GNOME positions, include a disclaimer.

Changing your email address or password
---------------------------------------

This can be done through your GNOME account. See :doc:`managing accounts </infrastructure/accounts/managing-accounts>`.

Email client configuration
--------------------------

The following clients are known to allow explicit From headers, which allows your email to appear as being from your @gnome.org alias. Web-based clients which do not allow From headers and which therefore cannot be used with a @gnome.org alias include: Yahoo! Mail and MSN Hotmail.

Evolution
~~~~~~~~~

To set an email alias in Evolution, create a new account with the following properties:

* Email Address: email alias (e.g name@gnome.org)
* Receiving Email: none
* Sending Mail: smtp.gnome.org, port 587 (TLS)
    * Server requires authentication
        * Username: your GNOME Account username
        * Password: your GNOME Account password 

Thunderbird
~~~~~~~~~~~

To set up an email alias in Thunderbird:

* Create an account for the email address that receives the redirected emails (for example, Gmail or any other email provider)
* Once done, go to Edit → Account Settings (or Tools → Account Settings), and select the account you just created.
* At the bottom right corner, click Manage Identities. Click on Add, and then fill in your name and your alias (e.g name@gnome.org).
* Make sure the outgoing mail server is set to smtp.gnome.org, port 587 TLS
    * Server requires authentication
        * Username: your GNOME Account username
        * Password: your GNOME Account password 
* Click OK

GMail
~~~~~

To use an @gnome.org alias using GMail's web interface:

* Go to Settings
* Go to Accounts → Imports
* Under, Send mail as click "Add another email address that you own"
* Enter the @gnome.org address and select "Consider this email as an alias"
* For the SMTP server, specify smtp.gnome.org, port 587 (TLS)
    * Server requires authentication
        * Username: your GNOME Account username
        * Password: your GNOME Account password 

In the case you want to send out an email from your @gnome.org address, on the New mail form make sure to select the @gnome.org address .

Geary
~~~~~

* Click Accounts in the main menu to open the Accounts editor
* Select the account you will be sending @gnome.org email from
* Click + at the bottom of the Email Addresses list and add your @gnome.org address
* Make sure the outgoing mail server is set to smtp.gnome.org, port 587 TLS
    * Server requires authentication
        * Username: your GNOME Account username
        * Password: your GNOME Account password 
