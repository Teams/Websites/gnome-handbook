Managing Accounts
=================

This page provides information on how to manage an existing :doc:`GNOME account </infrastructure/accounts>`.

Change email address
--------------------

The email address associated with your GNOME account is used for your gnome.org email alias, as well as notifications related to the account. To change the account email address:

* Login to GitLab.
* In your `GitLab profile email settings <https://gitlab.gnome.org/-/profile/emails>`_, set the secondary email address to the one you want to use for your GNOME account.
* `Open an account request issue <https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template=account-request>`_:
   * Set the title to "Account request" (case insensitive).
   * Using natural language, state that you want your email address to be updated. For example: "Please update my e-mail" will work.

Change password
---------------

To change your GNOME account password, use the page at `password.gnome.org <https://password.gnome.org/>`_.

Change your name or other personal details
------------------------------------------

The personal details associated with your GNOME account can be changed at `idm.gnome.org <https://idm.gnome.org>`_.

Manage two-factor authentication
--------------------------------

A two-factor authentication app, such as `Authenticator <https://apps.gnome.org/Authenticator>`__, is required when signing in with the GNOME single sign-on service (GNOME SSO). To set up or delete apps, go to the `Keycloak account security settings <https://sso.gnome.org/realms/master/account/account-security/signing-in>`__.