Teams
=====

Most GNOME components and apps have a developer team, which can be found in `GitLab <https://gitlab.gnome.org/>`_. Other project functions and roles have their own dedicated teams, which can be seen below:

.. list-table::
  :widths: 30 70
  :header-rows: 1

  * - Team
    - Role
  * - :doc:`Board of Directors </foundation/board-of-directors>`
    - Oversight and governance of the GNOME Foundation
  * - `Circle Committee <https://gitlab.gnome.org/Teams/Circle>`_
    - Runs the GNOME Circle initiative
  * - :doc:`Code of Conduct </foundation/committees/coc>`
    - Maintains the GNOME code of conduct
  * - `Design <https://gitlab.gnome.org/Teams/Design/>`_
    - User interface and UX design, icons, design guidelines
  * - `Documentation <https://gitlab.gnome.org/Teams/documentation>`_
    - Maintains GNOME's user and developer documentation
  * - `Engagement <https://gitlab.gnome.org/Teams/Engagement/>`_
    - Blogging, press, marketing and promotion
  * - `Infrastructure <https://gitlab.gnome.org/Infrastructure/Infrastructure/>`_
    - Maintains and develop GNOME's online infrastructure
  * - :doc:`Membership & Elections </foundation/committees/membership-and-elections>`
    - Membership applications and  Foundation Board elections
  * - :doc:`Release Team </release-planning/release-team>`
    - Development schedule, builds and makes releases
  * - `Translation <https://wiki.gnome.org/TranslationProject>`_ [#]_
    - Translates interfaces and docs into different languages
  * - :doc:`Travel Committee </foundation/committees/travel>`
    - Travel sponsorship for GNOME events
  * - `Websites <https://gitlab.gnome.org/Teams/Websites/>`_
    - Maintains and develops GNOME's websites

.. [#]  Also known as internationalization and localization, or i18n and l10n for short.
