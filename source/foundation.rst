Foundation
==========

The `GNOME Foundation <https://foundation.gnome.org/>`_ is the non-profit organization behind the GNOME Project. It is a membership-based organization, and every GNOME contributor is `entitled to be a member <https://foundation.gnome.org/membership/>`_, giving them a voice in how the Foundation is run. The Foundation provides leadership, financial backing, logistical support and legal services to the GNOME community. It also acts as a meeting point for the project's partners.

More information on the GNOME Foundation can be found on `its web pages <https://foundation.gnome.org/>`_, including: governance, legal and trademarks, membership and contact details.

Parts of the Foundation
-----------------------

* `Membership <https://foundation.gnome.org/membership/>`_ - every GNOME contributor is entitled to become a member.
* :doc:`Board of Directors </foundation/board-of-directors>` - responsible for governing the Foundation, elected annually by the membership.
* Advisory Board - fee-paying and non-fee-paying partners who regularly meet with the Board of Directors.
* Staff - the Foundation employs a small number of people, including an Executive Director, Director of Operations, and contractors.
* :doc:`Committees </foundation/committees>` - teams appointed by the board to do Foundation work

Support for community members
-----------------------------

In addition to managing key aspects of the GNOME project, the GNOME Foundation also provides support to community members. For example, there is financial and logistical help available. This includes:

.. list-table::
  :widths: 70 30
  :header-rows: 1

  * - Support available
    - Contact
  * - Reimbursement for travel and accommodation, particularly to conferences and hackfests
    - :doc:`Travel Committee </foundation/committees/travel>`
  * - Supplies for small events, such as release parties, workshops and hacking sessions. Can include drinks, snacks, speaker travel, equipment hire
    - `Request sponsorship <https://gitlab.gnome.org/Teams/Engagement/event-sponsorship-requests>`_
  * - Help finding venues for events
    - :doc:`Board of Directors </foundation/board-of-directors>`

If financial assistance is needed for something that isn't covered in this list, please don't hesitate to contact the Executive Director or Board of Directors.

Resources
---------

* `GNOME Foundation Bylaws <https://gitlab.gnome.org/Teams/Board/-/wikis/Bylaws>`_ - the Foundation's constitution, which legally defines how it is governed.
* `GNOME Foundation Charter <https://gitlab.gnome.org/Teams/Board/-/wikis/Charter>`_ - the original definition of the Foundation, which states its goals and purpose.
* `GNOME Foundation Annual Reports <https://foundation.gnome.org/reports/>`_

.. toctree::
   :hidden:

   foundation/board-of-directors
   foundation/committees
   foundation/membership-benefits
