Organize a Hackfest
===================

Any GNOME contributor can organize a hackfest. See below for details on how to go about it.

Requirements
------------

As the organiser of a hackfest, you are expected to:

* arrange a venue
* invite relevant contributors and ensure that people will attend
* create a page for the hackfest (a GNOME account is required to do this)
* provide advice on travel and accommodation
* ensure that the code of conduct is followed

Step by Step Guide
------------------

#. Figure out the basics:

   * Identify potential dates and venues.
   * Talk to the key attendees to confirm attendance.
   * Agree on the topics and goals for the event.

#. Create a new hackfest page:

   * Create a new Hedgedoc page with the name and year of the event as the URL, using the format ``https://hedgedoc.gnome.org/NAME-YEAR``. For example, ``https://hedgedoc.gnome.org/gtk-2000``.
   * Copy the content from :doc:`the template </events/hackfests/template>` into the new page that you've just created, and fill it in.
   * Submit an MR to this handbook, to have your hackfest listed on the :doc:`hackfests </events/hackfests>` page.

#. Once the details of the hackfest have been finalized, update the hackfest page.
#. Announce the hackfest on `Discourse <https://discourse.gnome.org/>`_, using the announcement tag.
#. If attendees are going to need :doc:`travel sponsorship </events/travel>`, encourage them to request travel sponsorship as soon as possible (ideally no later than 8 weeks before the hackfest).
#. If possible, reach out to the wider local community and investigate options to meet up with them. That could mean organizing a presentation for a local user group, or having a social event.
#. If you would like to advertise your hackfest, reach out to the Engagement Team.

You are encouraged to share news about the event, both during and after, using social media and blogging.

General Advice
--------------

* Start planning early. Last minute hackfests are more difficult to attend and are often more expensive. We recommend planning 2-6 months in advance.
* When choosing a location, consider who will be attending and the associated travel costs of getting them there.
* Consider who you need to be present to achieve the hackfest's goals, and make sure that they are able to attend.
* Think about the scope of the event, and try to avoid taking on too much. Remember that parallel tracks are only possible if the groups for each track are distinct.

Support
-------

The GNOME Foundation may be able to support your hackfest by helping you understand what's involved, identify and invite the right attendees, find sponsors and promote the event. If you need help, please email ``info@gnome.org``.

Code of Conduct
---------------

Hackfest organizers are responsible for ensuring that GNOME's `Code of Conduct <https://conduct.gnome.org/>`_ is enforced during their event.

* The hackfest page should contain a link to the Code of Conduct and should indicate that reports can be submitted to the Code of Conduct Committee.
* Organizers should familiarize themselves with the Code of Conduct. If you have any questions, contact the Code of Conduct Committee.
* For larger hackfests, you can make an announcement during the opening. Here you can mention that the events code of conduct should be followed and that the event organizers can be contacted about incidents.
