Project News
============

Getting News About GNOME
------------------------

The following are the main channels that you can follow for GNOME news:

* `@gnome <https://floss.social/@gnome>`_: for general project news and social media content
* `This Week in GNOME <https://thisweek.gnome.org/>`_: a weekly blog which summarizes the changes in GNOME over the past 7 days
* `Planet GNOME <https://planet.gnome.org/>`_: combines the blogs of GNOME contributors
* `Announcements on Discourse <https://discourse.gnome.org/tag/announcement>`_: official project announcements are made on Discourse (see :doc:`/get-in-touch/discourse` for how to subscribe)

Spread the Word
---------------

If you are a GNOME contributor, there are various ways to help generate news about the project:

* `Propose a social media post <https://gitlab.gnome.org/Teams/Engagement/Social-Media-and-News/-/issues/new>`_
* `Submit change details to This Week in GNOME <https://gitlab.gnome.org/World/twig/-/blob/main/README.md>`_
* :doc:`Create a blog on blogs.gnome.org </infrastructure/blog-hosting>`
* :doc:`Have your blog included in Planet GNOME </news/planet-gnome>`

.. toctree::
   :hidden:

   news/planet-gnome