Matrix
======

GNOME uses Matrix as its instant messaging platform. This page contains information on how to use it.

Accounts
~~~~~~~~

To chat in GNOME Matrix rooms, you first need to have a Matrix account. There are two ways to get one:

#. If you're not a GNOME Foundation member, you can register for a free account at `app.element.io <https://app.element.io/>`_.
#. GNOME Foundation members can register for a GNOME account at `gnome.element.io <https://gnome.element.io/>`_. To do this, register using an ``@gnome.org`` email address.

Once you have a Matrix account, you can login using a Matrix app, such as the Element web app.

Rooms
~~~~~

Once you are logged into Matrix, you can browse GNOME rooms and join the ones that are of interest to you. Some good ones to start with are:

.. list-table::
  :widths: 40 60
  :header-rows: 1

  * - Room
    - Topic
  * - `#gnome:gnome.org <https://matrix.to/#/#gnome:gnome.org>`_
    - General GNOME chat
  * - `#gnome-hackers:gnome.org <https://matrix.to/#/#gnome-hackers:gnome.org>`_
    - GNOME development discussion
  * - `#newcomers:gnome.org <https://matrix.to/#/#newcomers:gnome.org>`_
    - Help for newcomers to the GNOME project
