Discourse
=========

`discourse.gnome.org <https://discourse.gnome.org/>`_ is GNOME's main website for asynchronous communication (like email or a discussion forum). It is used for project announcements, general discussions, and support and technical questions.

Registering & Logging In
~~~~~~~~~~~~~~~~~~~~~~~~

To post or comment to GNOME's Discourse instance, you need to be logged in. This can be done with an existing Google or Github account. It is also possible to login with :doc:`a GNOME account </infrastructure/accounts>`.

.. _subscribing via email:

Subscribing via Email
~~~~~~~~~~~~~~~~~~~~~

It is possible to subscribe to categories and tags in Discourse, and to have notifications delivered via email. This can be a useful way to follow certain topics without having to have the site be open all the time.

To do this, you usually just need to:

1. Navigate to the category or tag you want to follow
2. Select "Watching" from the notifications menu 

More information can be found in `this Discourse post <https://discourse.gnome.org/t/interacting-with-discourse-via-email/46>`_.
